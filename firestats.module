<?php

/**
 * FireStats Drupal integration module.
 * Copyright Omry Yadan & Gurpartap Singh.
 * License: GPL GNU (http://www.gnu.org/copyleft/gpl.html).
 * Report issues/bugs at: http://drupal.org/project/issues/firestats
 */

function firestats_help($section) {
  switch ($section) {
    case 'admin/help#firestats':
      return 'Install <a href="http://firestats.cc/">FireStats</a>, then setup the <b>firestats path</b> in <em>'. l('firestats settings', 'admin/settings/firestats') . '</em>. Settings will be disabled unless you enter correct firestats path.';
      break;
    case 'admin/modules#description':
      return 'FireStats statistics engine integration module. You must install <a href="http://firestats.cc/" />firestats</a> to use this module.';
      break;
  }
}

function firestats_settings() {
  $attributes = file_exists(FS_INSTALL_PATH) ? '' : array('disabled' => 'disabled');
  $test = file_exists(FS_INSTALL_PATH) ? t('<img src="' . base_path() . '/misc/watchdog-warning.png" /><em> Firestats was found at: '. FS_SERVER_PATH . '</em>') : t('<img src="' . base_path() . '/misc/watchdog-error.png" /><em> Firestats path is not correct. Settings disabled.</em>');
  $roles = user_roles();
  $form = array();

  $form['firestats_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Firestats configuration'),
    '#collapsed' => TRUE,
  );
  $form['firestats_settings']['firestats_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Firestats path'),
    '#default_value' => variable_get('firestats_path', ''),
    '#description' => t('Enter the path relative to your server root. Without trailing slash.<br />Example: <em>/var/www/html/firestats</em>.'),
  );
  $form['firestats_settings']['firestats_test'] = array(
    '#type' => 'markup',
    '#value' => $test,
  );
  $form['firestats_settings_others'] = array(
    '#type' => 'fieldset',
    '#title' => t('Disable stats for:'),
    '#collapsed' => TRUE,
  );
  $form['firestats_settings_others']['firestats_admin_disable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Administration pages'),
    '#default_value' => variable_get('firestats_admin_disable', 0),
    '#attributes' => $attributes,
    '#description' => t('Checking this option will disable stats for administration pages. (i.e. <em>?q=admin/*</em>)'),
  );
  $form['firestats_settings_others']['firestats_role_disable'] = array(
    '#type' => 'select',
    '#title' => t('These roles'),
    '#options' => $roles,
    '#multiple' => TRUE,
    '#attributes' => $attributes,
    '#default_value' => variable_get('firestats_role_disable', ''),
    '#description' => t('Checking this option will disable stats for selected roles.'),
  );
  $form['firestats_settings_others']['firestats_users_disable'] = array(
    '#type' => 'textarea',
    '#title' => t('These users'),
    '#attributes' => $attributes,
    '#default_value' => variable_get('firestats_users_disable', NULL),
    '#description' => t('Stats will be disabled for entered users. Separate by new line or comma.'),
  );

  return $form;
}

/*function firestats_settings_form_validate($form_id, $form_values) {
  if (!$firestats_path = $form_values['firestats_path']) { //interupts reset to default option when path is incorrect
    form_set_error('firestats_path', 'FireStats path is not configured.');
  }
  if(!file_exists($firestats_path .  '/php/db-hit.php')) {
    form_set_error('firestats_path', 'FireStats was not found at <em>'. $firestats_path . '</em>');
  }
}*/

function firestats_init() {
  define('FS_FULL_INSTALLATION', '0');
  define('FS_SERVER_PATH', variable_get('firestats_path', ''));
  if (FS_SERVER_PATH != NULL)
    define('FS_INSTALL_PATH', FS_SERVER_PATH . '/php/db-hit.php');
}

function firestats_exit() {
  global $user;
  $udf = '';
  $rdf = '';
  $users_disabled_for = trim(variable_get('firestats_users_disable', NULL));
  if ($users_disabled_for != NULL) {
    $users_disabled_for = str_replace(array("\r\n", "\n", "\r"), ', ', $users_disabled_for);
    $users_disabled_for = str_replace(',', ', ', $users_disabled_for);
    $users_disabled_for = str_replace(',  ', ', ', $users_disabled_for);
    $users_disabled_for = explode(', ', $users_disabled_for);
    $udf = in_array($user->name, $users_disabled_for);
  }
  $roles_disabled_for = variable_get('firestats_role_disable', '');
  $rdf_status = '';
  if (count($roles_disabled_for) != 0) {
    foreach ($roles_disabled_for as $key => $value) {
      $rdf_status = array_key_exists($key, $user->roles);
    }
  }


  if((arg(0) == 'admin' && variable_get('firestats_admin_disable', 0)) || $udf || $rdf_status) {
    return;
  }
  elseif ($file_exists = file_exists(FS_INSTALL_PATH)) {
    require_once(FS_INSTALL_PATH);
    fs_add_hit(null,true);
    return;
  }
}
